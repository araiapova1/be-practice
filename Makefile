#lint will run mypy using mypy.ini config for your practice_1 folder
lint:
	mypy practice_1

#format will run black for your practice_1 folder
format:
	python3 -m black practice_1

#run-p1 will run your main.py file
run-p1:
	python3 practice_1/main.py
