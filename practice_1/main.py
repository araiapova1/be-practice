# Add to the main file.py the fibonacci function(n: int) -> int
# which is returning the fibonacci number for a given n


def fibonacci(n: int) -> int:
    if n <= 0:
        print("Your number should positive")

    elif n == 0:
        return 0

    elif n == 1 or n == 2:
        return 1

    return fibonacci(n - 1) + fibonacci(n - 2)


if __name__ == "__main__":
    assert fibonacci(1) == 1
    assert fibonacci(10) == 55
    assert fibonacci(14) == 377
